import React, { Component } from 'react';
import ReactDOM from 'react-dom';
 
import App from './App';
import reportWebVitals from './reportWebVitals';

  
   function Myfunction(){
    return (<div>
      
       <Clock/>
       
    </div>)
  }

class Clock extends React.Component{
  constructor(props){
    super(props)
    this.state = {date: new Date()}
    this.timer= null
  }
componentDidMount(){
  this.timer = setInterval(this.tick,1000)
}
//componentWillUnmount (){
  //window.clearInterval(this.timer)
//}
fun = e => {
 const heu= new Date(e.target.valueAsNumber)
  const h=heu.getHours()-1
  const m=heu.getMinutes()
  this.state.date.setHours(h)
  this.state.date.setMinutes(m)
  this.state.date.setSeconds(0)
  console.log(m)
}
fundate = e => {
const dat= new Date(e.target.value)
const mois=dat.getMonth()
const jour=dat.getDate()
const annee=dat.getFullYear()
this.state.date.setMonth(mois)
console.log(annee)
console.log(jour)

this.state.date.setDate(jour)
this.state.date.setFullYear(annee)
}
tick=()=>{

  var mod=this.state.date 
  var s=this.state.date.getSeconds()
   s=s+1
  mod.setSeconds(s)
  this.setState({date: mod})
}
forma() {
  var options = { year: 'numeric', month: 'long',day: 'numeric'}
  return <span>{this.state.date.toLocaleDateString([],options)}</span>
}
periode(){
  let heure=this.state.date.getHours()
  if (heure>=6 && heure<=11) {
    return <span className="periode">matin</span>
  }  
  if(heure>=12&&heure<=16){
    return <span className="periode">apres midi</span>
  }else{
    return <span className="periode">soir</span>
  }
}
jour(){
  let jour=this.state.date.getDay()
  if (jour==0) {
    return <span className="jour" >dimanche</span>
  }
  if (jour==1) {
    return <span className="jour">lundi</span>
  }
  if (jour==2) {
    return <span className="jour">mardi</span>
  }
  if (jour==3) {
    return <span className="jour">mecredi</span>
  }
  if (jour==4) {
    return <span className="jour">jeudi</span>
  }
  if (jour==5) {
    return <span className="jour">vendredi</span>
  }
  if (jour==6) {
    return <span className="jour">samedi</span>
  }
}

render(){
  return(<div className="content">
  <div className="App">
  
         {this.jour()}{this.periode()}  <span className="heure">{this.state.date.toLocaleTimeString()} </span> {this.forma() }
        <span> <label>modifier  l'heure</label><input type="time" name="modif" id="tyu" onChange={(e) =>this.fun(e)}/> </span>
         <label>modifier  la date</label><input type="date" name="modif" onChange={(e) =>this.fundate(e)}  /> 
  </div>
  </div>)
}
}
 
Clock.defaultProps = {
date: new Date()
 
}
ReactDOM.render(
   <Myfunction/>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
